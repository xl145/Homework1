import Fruit.fruits;
import Fruit.apple;
import Fruit.banana;
import Fruit.orange;
import Fruit.grape;
import java.util.Scanner;

public class Hwork {
	public static void main(String[] args){
		//Create Objects a,b,c,d and call their theColor() method;
		fruits a = new apple ("A");
		a.theColor();
		fruits b = new banana("B");
		b.theColor();
		fruits c = new orange("C");
		c.theColor();
		fruits d = new grape("D");
		d.theColor();
		
		System.out.println("Now you can enter A , B , C, D to see what color it is:");
		
		//Enter a letter from the keyboard (standard input),then press enter to see its color
		//can repeat entering the letter
		Scanner input = new Scanner (System.in);
		while(input.hasNextLine()){	
			String fruitName = input.nextLine();
			if (fruitName.equals ("A")){
				fruits e= new apple ("A");
				e.theColor();
			}
			else if(fruitName.equals("B")){
				fruits e= new banana ("B");
				e.theColor();
			}
			else if (fruitName.equals("C")){
				fruits e = new orange("C");
				e.theColor();
			}
			else if ( fruitName.equals("D")){
				fruits e= new grape ("D");
				e.theColor();
			}
			else {
				System.out.println("Sorry,"+fruitName +" is not a fruit.");
			}
			System.out.println();
			System.out.println("Now you can enter A , B , C, D to see what color it is:");
		}
		input.close();

	}
}
